(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
(package-initialize)

;; This is only needed once, near the top of the file
(eval-when-compile
  (require 'use-package))
(require 'bind-key) ;; Must be loaded dynamically

;; This disables TLS1.3 in emacs, which is not good, but apparently
;; it's currently broken.
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(blink-matching-paren-dont-ignore-comments t)
 '(custom-enabled-themes (quote (deeper-blue)))
 '(electric-pair-mode t)
 '(electric-pair-pairs
   (quote
    ((34 . 34)
     (8216 . 8217)
     (8220 . 8221)
     (40 . 41)
     (91 . 93))))
 '(find-ls-option (quote ("-exec ls -N -ld {} +" . "-dilsb")))
 '(package-selected-packages
   (quote
    (csv-mode flycheck flymake magit magit-lfs s slime slime-company vlf company org swiper use-package)))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
 '(global-semantic-idle-completions-mode t nil (semantic/idle))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(use-package tramp
  :ensure)

(use-package magit
  :ensure
  :bind (("C-x g" . magit-status)))

(use-package ivy
  :ensure
  :demand
  :functions ivy-mode
  :bind
  (("C-s"       . swiper)
   ("C-c C-r"   . ivy-resume)
   ;; ("<F6>"   . ivy-resume)
   ("M-x"       . counsel-M-x)
   ("C-x C-f"   . counsel-find-file)
   ("<f1> f"    . counsel-describe-function)
   ("<f1> v"    . counsel-describe-variable)
   ("<f1> l"    . counsel-find-library)
   ("<f2> i"    . counsel-info-lookup-symbol)
   ("<f2> u"    . counsel-unicode-char)
   ("C-c C-g"   . counsel-git)
   ("C-c C-j"   . counsel-git-grep)
   ("C-c C-k"   . counsel-ag)
   ("C-x C-l"   . counsel-locate)
   ;; ("C-S-o"  . counsel-rhythmbox)
   )
  :config
  (ivy-mode 1))

;; Tags with GGtags
(use-package ggtags
  :ensure
  :functions ggtags-mode
  :config
  (defun gg-c-hook ()
    (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
      (ggtags-mode 1)))
  :hook (c-mode-common-hook . gg-c-hook))

;; Stuff for C
(use-package cc-mode)
(use-package semantic
  :after cc-mode
  :functions (semantic-mode)
  :config
  (global-semanticdb-minor-mode 1)
  (global-semantic-idle-scheduler-mode 1)
  (global-semantic-decoration-mode 1)
  (global-semantic-highlight-func-mode 1)
  (global-semantic-idle-summary-mode 1)
  (semantic-mode 1))

;; Company-mode and completion functions
(use-package company
  :ensure
  :demand
  :hook (after-init-hook . global-company-mode))

;; Maximize Emacs
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Use ls emulation for dired
(use-package ls-lisp
  :config
  (setq dired-use-ls-dired nil
	ls-lisp-use-insert-directory-program nil))
